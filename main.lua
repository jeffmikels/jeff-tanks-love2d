--[[
This is my first code using the love2d.org game engine
and the lua programming language.

It was made by cloning the repository for this tutorial:
http://osmstudios.com/tutorials/your-first-love2d-game-in-200-lines-part-1-of-3

And then ripping everything apart.
--]]

-- todo
-- 1. bullet explosions
-- 2. turn-based play
-- 3. intro gui
-- 4. game reset
-- 5. artificial intelligence


-- LUA SETTINGS
math.randomseed(os.time())


-- SETUP GLOBALS
socket = require 'socket'
address, port = "*", 64738
server = {}
debug = true
game = {}

-- to match gravity to real physics
-- we want to know the pixel-to-meter ratio
game.options = {
	power_scale = 1,
	pixel_scale = 10,
	tank_scale = .5,
	initial_health = 100,
	gravity = 9.8 * 4,
	wind = 0,
	viscosity = 0,
}
game.state = 'playing'
game.current_tank = 1


-- GAME OBJECTS
-- Tank Constructor
-- a tank object will have name, color, intelligence type, position (x,y), health (1-100), power, angle, weapons inventory (type)
local newTank = function(conf)
	return {
		name = conf.name or 'Player',
		x = conf.x or 0,
		y = conf.y or 0,
		width = 60,
		height = 30,
		health = conf.health or game.options.initial_health,
		score = conf.score or 0,
		type = conf.type or 'human',
		angle = conf.angle or 0,
		inventory = conf.inventory or {},
		selected_weapon = 1,
		color = conf.color or {math.random(255),math.random(255),math.random(255)},
		power = (conf.health or game.options.initial_health) / 2
	}
end

-- Weapons Database
local Weapons = {
	{
		name = 'Tennis Ball',
		explosion_radius = 0,
		multiplier = 0,
		size = 3,
		damage = 0,
		distance_decay = 1,
		bounce_probability = .9,
		color = {255,255,0},
		min_health = -1,
	},
	{
		name = 'Cherry Bomb',
		explosion_radius = 28,
		multiplier = 10,
		size = 30,
		damage = 100,
		distance_decay = 1,
		bounce_probability = .2,
		color = {200, 100, 100},
		min_health = 15,
		image = 'img/cherrybomb.svg',
		sound = '',
	},
	{
		name = 'Cannonball',
		explosion_radius = 20,
		multiplier = 3,
		size = 5,
		damage = 150,
		distance_decay = .6,
		bounce_probability = .5,
		min_health = 20,
		sound = ''
	},
	{
		name = 'Missile',
		explosion_radius = 40,
		multiplier = 1,
		size = 30,
		damage = 200,
		distance_decay = .05,
		bounce_probability = 0,
		color = {100, 100, 200},
		min_health = 80,
		image = 'img/Blue-rocket.svg',
		sound = '',
	}
}

-- Helper Functions
local keyDown = love.keyboard.isDown
local fix_y = function(y)
	return (-1) * (y - love.graphics.getHeight())
end
local radians = function(angle)
	-- because y is inverted, radians go CLOCKWISE
	return angle * (math.pi / 180)
end
local function log(s)
	print('LOG: ' .. tostring(s))
end


-- Images Storage
images = {}

-- Audio Storage
sounds = {}

-- Entity Storage
projectiles = {} 	-- array of current bullets being drawn and updated
tanks = {} 			-- array of current tanks on screen

-- Ground Data
ground = {}

-- simple storage for first time
player = {}
enemy = {}
bullet = {}
explosion = {}
winner = {}


-- General Game Functions

-- Collision detection taken function from http://love2d.org/wiki/BoundingBox.lua
-- Returns true if two boxes overlap, false if they don't
-- x1,y1 are the left-top coords of the first box, while w1,h1 are its width and height
-- x2,y2,w2 & h2 are the same, but for the second box
function CheckCollision(x1,y1,w1,h1, x2,y2,w2,h2)
  return x1 < x2+w2 and
         x2 < x1+w1 and
         y1 < y2+h2 and
         y2 < y1+h1
end

function overlaps(obj1, obj2)
	if obj1.alive and obj2.alive then
		return CheckCollision(obj1.body.x, obj1.width, obj1.height, obj2.x, obj2.y, obj2.width, obj2.height)
	else
		return false
	end
end

function checkHit(bullet, tank)
	if bullet.alive and tank.alive then
		return CheckCollision(bullet.x, bullet.y, bullet.width, bullet.height, tank.body.min_x, tank.body.min_y, tank.body.max_x - tank.body.min_x, tank.body.max_y - tank.body.min_y)
	else
		return false
	end
end


function kill(obj)
	if obj.alive then
		obj.alive = false
	end
end

function explode(obj, radius)
	if obj.alive then
		obj.alive = false
		explosion.x = obj.x
		explosion.y = obj.y
		explosion.radius = radius
		explosion.start_time = love.timer.getTime()
		explosion.alive = true
		explosion.status = 'exploding'
	end
end


-- place tanks
function placeTanks()
	local numregions = table.getn(tanks)
	local region_width = love.graphics.getWidth() / numregions

	local unplaced_tanks = {}
	for i, tank in ipairs(tanks) do
		table.insert(unplaced_tanks, tank)
	end

	new_tank_table = {}

	for i=1,numregions do
		-- select a random tank for the first region
		local r = math.random(table.getn(unplaced_tanks))
		tank = unplaced_tanks[r]

		-- select a random x in that region
		tank.x = math.random((i-1) * region_width, i * region_width)

		-- place the tank on the ground
		tank.y = math.min(ground[tank.x], love.graphics.getHeight())
		
		-- compute tank body
		local tanktemplate = {{-tank.width/2, tank.height / 3}, {-tank.width/2, -tank.height/3}, {-tank.width/3, -tank.height/3}, {-tank.width/6, -tank.height*2/3}, {tank.width/6, -tank.height*2/3}, {tank.width/3, -tank.height/3}, {tank.width/2, -tank.height/3}, {tank.width/2, tank.height/3}}
		tank.body = {}
		tank.body.coords = {}
		tank.body.min_x = love.graphics.getWidth()
		tank.body.min_y = love.graphics.getHeight()
		tank.body.max_x = 0
		tank.body.max_y = 0
		for i, point in ipairs(tanktemplate) do
			xpoint = tank.x + point[1] * game.options.tank_scale
			ypoint = tank.y + point[2] * game.options.tank_scale
			table.insert(tank.body.coords, xpoint)
			table.insert(tank.body.coords, ypoint)
			tank.body.min_x = math.min(tank.body.min_x, xpoint)
			tank.body.min_y = math.min(tank.body.min_y, ypoint)
			tank.body.max_x = math.max(tank.body.max_x, xpoint)
			tank.body.max_y = math.max(tank.body.max_y, ypoint)
		end
		
		-- enliven tank
		tank.alive = true

		table.insert(new_tank_table, tank)

		-- remove from the unplaced_tanks table
		table.remove(unplaced_tanks, r)
	end

	-- change the tanks table to reflect the new ordering
	tanks = new_tank_table
end

function nextTank()
	log(player.alive)
	log(player.name)
	if not player or not player.name then
		log('picking random tank')
		game.current_tank = math.random(table.getn(tanks));
		log(game.current_tank)
		player = tanks[game.current_tank]
	else
		player = {}
		while not player.alive do
			log('selecting next alive tank')
			game.current_tank = math.fmod(game.current_tank + 1, table.getn(tanks))
			-- lua tables are indexed from 1
			if (game.current_tank == 0) then game.current_tank = table.getn(tanks) end
			log(game.current_tank)
			player = tanks[game.current_tank]
		end
	end
end


-- generate mountain
function makeMountains(canvas, options)
	-- copied from http://cs.lmu.edu/~ray/notes/canvas/
	local width = love.graphics.getWidth()
	local height = love.graphics.getHeight()

	-- // setup defaults
	options = options or {}
	options.ruggedness = options.ruggedness or math.random() * 0.75
	options.color = options.color or {74, 37, 0}
	options.foreground = options.foreground or true
	options.height = options.height or game.options.mountain_max_height or height * .75

	-- set up table for mountain polygon points
	local mountain_points = {}
	-- mountain_points = {0, height}
	local x = 0
	local y = options.height
	-- table.insert(mountain_points, x)
	-- table.insert(mountain_points, y)

	-- // if this is a foreground mountain, we modify the game ground values
	-- // the game ground is allowed to be one pixel below the bottom border
	if options.foreground then
		ground[math.floor(x)] = math.max(-1,y)
	end

	fractalLine(mountain_points, x, y, width, y, options)

	-- close the polygon
	-- table.insert(mountain_points, width)
	-- table.insert(mountain_points, height)
	-- table.insert(mountain_points, 0)
	-- table.insert(mountain_points, height)
	-- table.insert(mountain_points, x)
	-- table.insert(mountain_points, y)

	-- love has problems rendering complex polygons, let's do it manually.
	love.graphics.setCanvas(canvas)
	love.graphics.setColor(options.color)
	-- love.graphics.polygon('fill', mountain_points)

	love.graphics.setLineWidth(5)
	for i=0,width do
		love.graphics.line(i, height, i, ground[i] + 2)
	end


	if options.foreground then
		love.graphics.setColor(0,0,0)
		love.graphics.setLineWidth(5)
		-- love.graphics.polygon('line', mountain_points)
		-- love.graphics.line(mountain_points)
	end

	-- reset the canvas to default
	love.graphics.setCanvas()
	love.graphics.setLineWidth(1)
end

-- // Draws a fractal line by recursive decomposition, using the global ruggedness
-- // value.  The line is drawn from (x,y) to (tox,toy).  After the line is drawn,
-- // (x,y) is updated to the value of (tox, toy).
function fractalLine (points, x, y, tox, toy, options)
    if x + 1 > tox then
        x = tox
		y = toy
		table.insert(points, x)
		table.insert(points, y)
		if (options.foreground) then ground[math.floor(x)] = math.max(-1,y) end
    else
        local midx = (x + tox) / 2
        local midy = (y + toy) / 2 + randScale(options.ruggedness * (tox - x))
        local retObj
		retObj = fractalLine(points, x, y, midx, midy, options)
		x = retObj.x
		y = retObj.y

        retObj = fractalLine(points, x, y, tox, toy, options)
		x = retObj.x
		y = retObj.y
	end
	return {x = x, y = y}
end

function randScale(x)
	return -(x/2.0) + x * math.random()
end

-- Loading
function resetGame()
	setupGame()
end

function setupGame()
	-- setup mountains
	background.canvas:clear()

	-- makeMountains(background.canvas, {foreground=false})
	makeMountains(background.canvas)

	-- setup two tanks
	t1 = newTank({name = 'Jeff'})
	t2 = newTank({name = 'Enemy', type = 'random'})
	-- t2 = newTank({name = 'Enemy', type = 'net'})
	tanks = {t1, t2}

	placeTanks()

	--nextTank()
	player = tanks[1]
	game.state = 'playing'
end

function love.load(arg)
	-- set socket listener
	server = socket.udp()
	server:settimeout(0)
	server:setsockname(address, port)
	log('Listening on UDP port ' .. tostring(port))
	
	-- setup fonts
	titleFont = love.graphics.newFont("assets/NotoSans.ttf", 64);
	statusFont = love.graphics.newFont();
	
	-- set main background
	love.graphics.setBackgroundColor(200,200,255)
	love.graphics.setColorMask()

	-- setup mountain canvas
	background = {}
	background.canvas = love.graphics.newCanvas()


	-- load images
	images.player = love.graphics.newImage('assets/plane.png')
	images.enemy = love.graphics.newImage('assets/enemy.png')
	images.bullet = love.graphics.newImage('assets/bullet.png')

	-- load sounds
	sounds.gun = love.audio.newSource('assets/gun-sound.wav', 'static')

	-- setup bullet
	bullet.img = images.bullet
	bullet.width = images.bullet:getWidth()
	bullet.height = images.bullet:getHeight()
	bullet.x = -10
	bullet.y = -10
	bullet.alive = false
	bullet.angle = 90
	bullet.status = 'ready'
	bullet.explosion_radius = 10

	setupGame()

	-- setup player on left of screen
	-- player.img = images.player
	-- player.width = images.player:getWidth()
	-- player.height = images.player:getHeight()
	-- player.x = 50
	-- player.y = love.graphics.getHeight() - 50
	-- player.angle = 0
	-- player.power = 50
	-- player.health = game.options.initial_health
	-- player.name = "Jeff"
	-- player.alive = true
	-- player.color = {200,100,100}
	--
	-- -- setup enemy tank on right of screen
	-- enemy.img = images.enemy
	-- enemy.width = images.enemy:getWidth()
	-- enemy.height = images.enemy:getHeight()
	-- enemy.x = love.graphics.getWidth() - 50
	-- enemy.y = love.graphics.getHeight() - 50
	-- enemy.angle = 90
	-- enemy.alive = true


end


-- Updating
function incPlayerPower(increment)
	player.power = math.min(player.power + increment, player.health)
end

function incPlayerAngle(increment)
	-- because the y axis is flipped, unit circles go clockwise
	-- this method makes sure angles are always positive
	player.angle = math.fmod(player.angle + 360 + increment, 360)
end

function doFire()
	bullet.v = player.power * game.options.power_scale * game.options.pixel_scale
	bullet.angle = math.fmod(player.angle + 360, 360)
	bullet.radians = math.rad(bullet.angle)
	bullet.rotation = math.rad(player.angle)

	bullet.x = player.x + math.cos(math.rad(player.angle)) * (20 + player.power * game.options.tank_scale / 2)
	bullet.y = player.y + math.sin(math.rad(player.angle)) * (20 + player.power * game.options.tank_scale / 2)

	-- remember that the radians in this coordinate system go CLOCKWISE
	bullet.vx = bullet.v * math.cos(bullet.radians)
	bullet.vy = bullet.v * math.sin(bullet.radians)
	bullet.alive = true
	bullet.status = 'firing'
	bullet.source = player
end

function doQuit()
	love.event.push('quit')
end


function love.update(dt)

	-- look for alive tanks
	local alive_tanks = 0
	local maybe_winner
	for i, tank in ipairs(tanks) do
		if tank.alive then
			alive_tanks = alive_tanks + 1
			maybe_winner = tank
		end
	end
	if alive_tanks == 1 then
		game.state = 'ended'
		winner = maybe_winner
	end

	-- I always start with an easy way to exit the game
	if love.keyboard.isDown('escape') then
		doQuit()
		return
	end

	if game.state == 'playing' then

		if bullet.status == 'done' then
			nextTank()
			bullet.status = 'ready'
		end

		-- player = tanks[game.current_tank]

		-- if a bullet is ready, don't read player controls
		if bullet.status == 'ready' then
			
			-- read human controls from the keyboard
			if player.type == 'human' then
				-- handle rotation of player
				local increment = 1
				if keyDown('lshift','rshift') then
					increment = .1
				end
					
				if keyDown('right','d') then
					incPlayerAngle(increment)
				end
				if keyDown('left','a') then
					incPlayerAngle(-increment)
				end

				-- handle power of player
				if keyDown('up', 'w') then
					incPlayerPower(increment)
				end
				if keyDown('down', 's') then
					incPlayerPower(-increment)
				end
				
				-- handle firing
				if love.keyboard.isDown(' ') and bullet.alive == false then
					doFire()
				end
			elseif player.type == 'random' then
				player.power = math.random(player.health)
				player.angle = math.random(181,359)
				doFire()
			elseif player.type == 'net' then
				data, msg, port = server:receivefrom()
				if data then
					log('data received: ' .. data)
					if data == 'up' then
						incPlayerPower(1)
					end
					if data == 'shift-up' then
						incPlayerPower(.1)
					end
					if data == 'down' then
						incPlayerPower(-1)
					end
					if data == 'shift-up' then
						incPlayerPower(-.1)
					end
					if data == 'right' then
						incPlayerAngle(1)
					end
					if data == 'shift-right' then
						incPlayerAngle(.1)
					end
					if data == 'left' then
						incPlayerAngle(-1)
					end
					if data == 'shift-left' then
						incPlayerAngle(-.1)
					end
					if data == '[fire]' then
						doFire()
					end
				end
			end

		end

		-- handle projectile motion
		if bullet.alive then
			-- remember that vy is negative going up
			bullet.vy = bullet.vy + (game.options.gravity * game.options.pixel_scale * dt)
			bullet.y = bullet.y + bullet.vy * dt
			bullet.x = bullet.x + bullet.vx * dt

			-- now handle the rotation of the bullet sprite
			local rotation_correction = math.pi/2
			if bullet.vx < 0 then rotation_correction = - math.pi / 2 end
			bullet.rotation = math.atan(bullet.vy / bullet.vx) + rotation_correction
		end

		-- handle collisions
		if bullet.alive then

			-- look for collisions with tanks
			local hit = false
			for i, tank in ipairs(tanks) do
				if checkHit(bullet, tank) and bullet.status ~= 'firing' then
					kill(tank)
					explode(bullet, bullet.explosion_radius)
					hit = true
				end
			end
			if not hit then
				bullet.status = 'flying'
			end

			-- explode bullet if it hit the ground
			-- print (bullet.x .. ' ' .. ground[math.floor(bullet.x)])
			if ground[math.floor(bullet.x)] and bullet.y > ground[math.floor(bullet.x)] then
				explode(bullet, bullet.explosion_radius)
			end

			-- stop drawing bullet if it is below the border
			if bullet.y > love.graphics.getHeight() then
				bullet.status = 'done'
				bullet.alive = false
			end
		end
	end

	if game.state == 'ended' then
		if love.keyboard.isDown('r') then
			resetGame()
		end
	end


	if not player.alive and love.keyboard.isDown('r') then
		-- remove all our bullets and enemies from screen
		-- bullets = {}
		-- enemies = {}

		-- move player back to default position
		-- player.x = 50
		-- player.y = 710

		-- reset our game state
		score = 0
		isAlive = true
	end
end



-- Drawing
function doStatusBar()
	local pn = 'Player: ' .. tostring(player.name) .. ' (' .. tostring(player.health) .. ')'
	local weapon = '[ weapon ]'
	local power = 'P: ' .. tostring(player.power)
	local angle = 'A: ' .. tostring(player.angle)
	love.graphics.setColor(0,0,0)
	if bullet.alive then love.graphics.setColor(255,50,50) end
	love.graphics.rectangle('fill', 0, 0, love.graphics.getWidth(), 30)
	love.graphics.setFont(statusFont)
	love.graphics.setColor(player.color)
	love.graphics.print(pn, 20, 10)
	love.graphics.setColor(255, 255, 255)
	love.graphics.print(power, 200, 10)
	love.graphics.print(angle, 260, 10)
	love.graphics.print(weapon, 320, 10)
end

function drawTanks()
	for i, tank in ipairs(tanks) do
		if tank.alive then
			if i == game.current_tank then
				drawTank(tank, true)
			else
				drawTank(tank, false)
			end
		end
	end
end

function drawTank(obj, highlight)
	-- draws a tank shape with the details of the obj argument
	local highlight = highlight or false

	-- if the tank is highlighted, draw that first
	if highlight then
		love.graphics.setColor({255,255,0, 160})
		love.graphics.circle('fill', obj.x, obj.y, 40)
	end

	-- draw the turret
	local radians = math.rad(obj.angle)
	local turret_size = 20 + obj.power * game.options.tank_scale / 2
	love.graphics.setColor(obj.color or {255,255,255})
	love.graphics.arc('fill', obj.x, obj.y, turret_size, radians - math.pi/turret_size, radians + math.pi/turret_size)
	love.graphics.setColor(0,0,0)
	love.graphics.arc('line', obj.x, obj.y, turret_size, radians - math.pi/turret_size, radians + math.pi/turret_size)

	-- draw the tank body
	love.graphics.setColor(obj.color or {255,255,255})
	love.graphics.polygon('fill', obj.body.coords)
	love.graphics.setColor(0,0,0)
	love.graphics.polygon('line', obj.body.coords)
	
	-- draw the tank debug
	-- love.graphics.setColor(255,0,0)
	-- love.graphics.rectangle('line', obj.body.min_x, obj.body.min_y, obj.body.max_x - obj.body.min_x, obj.body.max_y - obj.body.min_y)
end

function drawBackground()
	love.graphics.setColor(255,255,255)
	love.graphics.draw(background.canvas,0,0)
end

function drawExplosion()
	if explosion.alive then
		local explosion_time = .5
		local elapsed_time = love.timer.getTime() - explosion.start_time
		if elapsed_time >= explosion_time then
			explosion.alive = false
			explosion.status = 'done'
			bullet.status = 'done'
		else
			local current_radius = 20 + explosion.radius * elapsed_time / explosion_time
			-- print('drawing explosion of radius: ' .. tostring(current_radius))
			love.graphics.setColor(255,0,0)
			love.graphics.circle('fill', explosion.x, explosion.y, current_radius)
		end
	end
end

function drawBullet()
	if bullet.alive then
		love.graphics.draw(bullet.img, bullet.x, bullet.y, bullet.rotation, 1, 1, bullet.width / 2, bullet.height / 2)
	end
end

function drawEnd()
	love.graphics.setFont(titleFont)
	love.graphics.print(winner.name .. " wins!", 100, 100)
	love.graphics.setFont(statusFont)
end


function love.draw(dt)
	drawBackground()

	if game.state == 'ended' then
		drawEnd()
	elseif game.state == 'playing' then
		doStatusBar()
		drawTanks()
		drawBullet()
		drawExplosion()
	end


	if debug then
		fps = tostring(love.timer.getFPS())
		-- love.graphics.print("Current FPS: "..fps, love.graphics.getWidth() - 30, 10)
	end
end

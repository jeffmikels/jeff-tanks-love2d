-- Configuration
function love.conf(t)
	t.title = "Jeff's Tanks" -- The title of the window the game is in (string)
	t.version = "0.9.2"         -- The LÖVE version this game was made for (string)
	t.window.width = 1024
	t.window.height = 768

	-- For Windows debugging
	t.console = true
end
